<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Working store id.
     *
     * @var int
     */
    protected $_storeId;

    /**
     * Check whether or not we are in the admin panel.
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml');
    }

    /**
     * Get working store id, works for frontend and backend.
     *
     * @return int
     */
    public function getStoreId()
    {
        if (is_null($this->_storeId)) {
            if ($this->isAdmin()) {
                $store = Mage::app()->getRequest()->getParam('store');

                if (!is_numeric($store)) {
                    $storeModel = Mage::getModel('core/store')->load($store);

                    if ($storeModel && $storeModel->getId()) {
                        $this->_storeId = (int)$storeModel->getId();
                    }
                } else {
                    $this->_storeId = (int)$store;
                }
            } else {
                $this->_storeId = (int)Mage::app()->getStore()->getId();
            }
        }

        return (int)$this->_storeId;
    }

    /**
     * Check whether or not we are in test mode.
     *
     * @return bool
     */
    public function isTestMode()
    {
        return Mage::getStoreConfigFlag('payment/hostedflow_standard/test');
    }

    /**
     * Log debug message.
     *
     * @param string|array|Exception $message
     * @return $this
     * @throws Exception
     */
    public function debugLog($message)
    {
        Mage::helper('hostedflow/log')->entry($message);

        return $this;
    }

    /**
     * Retrieve completely random string.
     *
     * @param int $length
     * @param string $charset
     * @return string
     */
    public function strRand($length, $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    {
        $result = '';

        $length = (int)$length;

        if ($length > 0) {
            $max = strlen($charset)-1;

            for ($i = 0; $i < $length; $i++) {
                $result.= $charset[mt_rand(0, $max)];
            }
        }

        return $result;
    }

    /**
     * Load order using id.
     *
     * @param int $id
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function getOrderById($id)
    {
        /** @var int $id */
        $id = (int)$id;

        if (!$id) {
            throw new Exception('Invalid or missing order id.');
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {
            throw new Exception('Failed to load order with id ' . $id);
        }

        return $order;
    }

    /**
     * Load order using hostedflow_token value.
     *
     * @param string $token
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function getOrderByToken($token)
    {
        /** @var string $token */
        $token = (string)$token;

        if (empty($token)) {
            throw new Exception('Invalid or missing order token.');
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->loadByAttribute('hostedflow_token', $token);

        if (!$order->getId()) {
            throw new Exception('Failed to load order with token ' . $token);
        }

        return $order;
    }

    /**
     * Retrieve SOAP based API model.
     *
     * Ws use a singleton implementation since the calls aren't dependant on data stored within the object instance.
     *
     * @return Resursbank_Hostedflow_Model_Api
     */
    public function getApi()
    {
        return Mage::getSingleton('hostedflow/api');
    }

    /**
     * Retrieve CURL based API model meant for gateway specific calls.
     *
     * We use a model since it's anyways only used from app/code/community/Resursbank/Hostedflow/controllers/PaymentController.php
     * right now. If this changes we may want to consider using a singleton implementation instead, but then we should
     * review the model carefully first.
     *
     * @return Resursbank_Hostedflow_Model_Gateway
     */
    public function getGateway()
    {
        return Mage::getModel('hostedflow/gateway');
    }

    /**
     * Check whether or not module is enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag('payment/hostedflow_standard/active');
    }

    /**
     * Checks if the provided $path matches the current module/controller/action path.
     *
     * @param string $path (eg. catalog/product/view)
     * @return bool
     */
    public function isActivePath($path)
    {
        return ((Mage::app()->getRequest()->getModuleName() . '/' . Mage::app()->getRequest()->getControllerName() . '/' . Mage::app()->getRequest()->getActionName()) === $path);
    }

    /**
     * Retrieve suggested part payment price. This will be displayed on the product page below the product price.
     *
     * @param float $price
     * @return float
     */
    public function getSuggestedPartPaymentPrice($price)
    {
        $result = 0;

        try {
            $price = (float)$price;

            if ($this->canUsePartPayment($price)) {
                /** @var string $paymentMethod */
                $paymentMethod = $this->getSuggestedPartPaymentMethod();

                if (!empty($paymentMethod)) {
                    /** @var array $factor */
                    $factor = $this->getAnnuityFactor($paymentMethod, $this->getPartPaymentSuggestionDuration());

                    if (isset($factor['factor'])) {
                        $result = round(($price * (float)$factor['factor']), 2);
                    }
                }
            }
        } catch (Exception $e) {
            $this->debugLog($e);
        }

        return (float)$result;
    }

    /**
     * Retrieve annuity factor based on payment method and duration.
     *
     * @param string $paymentMethod
     * @param int $duration
     * @return array
     */
    public function getAnnuityFactor($paymentMethod, $duration)
    {
        $paymentMethod = (string)$paymentMethod;
        $duration = (int)$duration;

        $cacheKey = $this->getCache()->generateCacheKey(array(
            __CLASS__,
            __FUNCTION__,
            $paymentMethod,
            $duration
        ));

        /** @var array $result */
        $result = $this->getCache()->decode($this->getCache()->load($cacheKey), true);

        if (is_null($result)) {
            $factors = $this->getApi()->getAnnuityFactors(array(
                'paymentMethodId' => $paymentMethod
            ));

            if (is_array($factors) && count($factors)) {
                /** @var array $factor */
                foreach ($factors as $factor) {
                    if (is_array($factor) &&
                        isset($factor['factor']) &&
                        isset($factor['duration']) &&
                        (int)$factor['duration'] === $duration
                    ) {
                        $result = $factor;
                        break;
                    }
                }
            }

            // Store result in cache for future calls.
            $this->getCache()->save($this->getCache()->encode($result), $cacheKey);
        }

        return is_array($result) ? $result : array();
    }

    /**
     * Retrieve available payment methods from cache (if there are none stored in the cache we will retrieve them
     * through an API call and store them in cache for future calls).
     *
     * @param array $parameters
     * @param null|string|array $filters
     * @return array
     */
    public function getPaymentMethods(array $parameters = array(), $filters = null)
    {
        $cacheKey = $this->getCache()->generateCacheKey(array(
            __CLASS__,
            __FUNCTION__,
            $parameters
        ));

        /** @var array $methods */
        $result = $this->getCache()->decode($this->getCache()->load($cacheKey), true);

        if (is_null($result)) {
            $result = $this->getApi()->getPaymentMethods(array_merge($parameters, array(
                'language' => substr(Mage::app()->getLocale()->getLocaleCode(), 0, 2),
                'customerType' => $this->getCustomerType()
            )));

            $this->getCache()->save($this->getCache()->encode($result), $cacheKey);
        }

        return $this->filterPaymentMethods($result, $filters);
    }

    /**
     * Filter response from getPaymentMethods().
     *
     * @param array $methods
     * @param null|string|array $filters
     * @return array
     */
    public function filterPaymentMethods(array $methods, $filters = null)
    {
        $result = $methods;

        if (!is_null($filters)) {
            $filters = (array)$filters;

            if (count($filters)) {
                foreach ($result as $index => $method) {
                    if (isset($method['id']) && !in_array($method['id'], $filters)) {
                        unset($result[$index]);
                    }
                }
            }
        }

        return array_values($result);
    }

    /**
     * Returns the number of months to use for suggested part payments prices.
     *
     * @return int
     */
    public function getPartPaymentSuggestionDuration()
    {
        return (int)Mage::getStoreConfig('payment/hostedflow_standard/part_payment_suggestion_duration');
    }

    /**
     * Check if a price is eligible for part payments.
     *
     * @param float $price
     * @return bool
     */
    public function canUsePartPayment($price)
    {
        $price = (float)$price;

        return (
            ($price > 0) &&
            ($price >= (float)Mage::getStoreConfig('payment/hostedflow_standard/min_order_total')) &&
            ($price <= (float)Mage::getStoreConfig('payment/hostedflow_standard/max_order_total'))
        );
    }

    /**
     * Retrieve payment method to base suggested part payment prices upon.
     *
     * @return string
     */
    public function getSuggestedPartPaymentMethod()
    {
        return (string)Mage::getStoreConfig('payment/hostedflow_standard/part_payment_suggestion_method');
    }

    /**
     * Retrieve customer type (companies = LEGAL otherwise NATURAL)
     *
     * @return string
     */
    public function getCustomerType()
    {
//        return $this->isUserCompany() ? 'LEGAL' : 'NATURAL';

        // @todo the above code works, however, when we submit customerType "LEGAL" to the API we get an empty response but no errors. I'm not sure what that means, investigate further before completing implementation.
        return 'NATURAL';
    }

    /**
     * Check if customer is logged in on a company based account.
     *
     * @return bool
     */
    public function isUserCompany()
    {
        return (Mage::getSingleton('customer/session')->isLoggedIn() && Mage::getSingleton('customer/session')->getCustomer()->getDefaultBillingAddress() && Mage::getSingleton('customer/session')->getCustomer()->getDefaultBillingAddress()->getCompany());
    }

    /**
     * Retrieve cache helper.
     *
     * @return Resursbank_Hostedflow_Helper_Cache
     */
    public function getCache()
    {
        return Mage::helper('hostedflow/cache');
    }

    /**
     * Rebuild cart contents from existing order.
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function rebuildCart(Mage_Sales_Model_Order $order)
    {
        /* @var $cart Mage_Checkout_Model_Cart */
        $cart = Mage::getSingleton('checkout/cart');

        /** @var Mage_Sales_Model_Resource_Order_Item_Collection $items */
        $items = $order->getItemsCollection();

        if (count($items)) {
            foreach ($items as $item) {
                $cart->addOrderItem($item);
            }
        }

        $cart->save();

        return $this;
    }

    /**
     * Cancel an order and mark it with a custom order status to indicate it was cancelled from gateway (either because
     * the client choose to go back to the checkout and modify the contents of their cart, or because their payment was
     * rejected by Resurs Bank).
     *
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function cancelOrder(Mage_Sales_Model_Order $order)
    {
        $order->cancel()->setStatus('resursbank_canceled')->save();

        return $this;
    }

    /**
     * Retrieve payment information.
     *
     * @param Mage_Sales_Model_Order $order
     * @return \Resursbank\RBEcomPHP\resurs_payment
     */
    public function getPayment(Mage_Sales_Model_Order $order)
    {
        return $this->_getEcomHelper()->getConnection()->getPayment($order->getIncrementId());
    }

    /**
     * @return Resursbank_Hostedflow_Helper_Ecom
     */
    public function _getEcomHelper()
    {
        return Mage::helper('hostedflow/ecom');
    }

}
