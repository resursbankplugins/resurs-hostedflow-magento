<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Helper_Cache extends Mage_Core_Helper_Abstract
{

    const CACHE_TAG = 'hostedflow';

    /**
     * @var Mage_Core_Model_Cache
     */
    protected $_cache;

    /**
     * Store data in cache.
     *
     * @param mixed $data
     * @param string $id
     * @param array $tags
     * @param int $lifetime (in seconds)
     * @param boolean $register (whether or not to store data in registry as well)
     * @return $this
     */
    public function save($data, $id, $tags = array(), $lifetime = null, $register = false)
    {
        try {
            if ($this->isEnabled()) {
                $this->debugLog("Saving cache with id {$id}");

                if (is_null($lifetime)) {
                    $lifetime = $this->getLifetime();
                }

                $lifetime = (int)$lifetime < 1 ? null : $lifetime;

                $tags[] = self::CACHE_TAG;

                $this->getCache()->save($data, $id, array_unique($tags), $lifetime);
            }

            if ($register) {
                Mage::register($id, $data);
            }
        } catch (Exception $e) {
            $this->debugLog($e);
        }

        return $this;
    }

    /**
     * Load cached data.
     *
     * @param string $id
     * @param boolean $register (whether or not to load data from registry if nothing is found in the cache).
     * @return mixed
     */
    public function load($id, $register = false)
    {
        $result = null;

        if ($this->isEnabled()) {
            // debug log
            $this->debugLog("Loading cached data with id {$id}");

            // load data from cache
            $result = $this->getCache()->load($id);
        }

        if (is_null($result) && $register) {
            $result = Mage::registry($id);
        }

        return $result;
    }

    /**
     * Get cache model, with correct options (we must take this route to account for custom cache engines such as Redis).
     *
     * @return Mage_Core_Model_Cache
     */
    public function getCache()
    {
        if (is_null($this->_cache)) {
            $this->_cache = Mage::app()->getCacheInstance();
        }

        return $this->_cache;
    }

    /**
     * Check if cache is enabled.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::app()->useCache(self::CACHE_TAG);
    }

    /**
     * Clear cache data.
     *
     * @param null|string $id
     * @return $this
     */
    public function clean($id = null)
    {
        try {
            if (is_string($id) && !empty($id)) {
                $this->debugLog("Cleaning out cached data with id {$id}");

                $this->getCache()->remove($id);
            } else {
                $this->debugLog("Cleaning out cached data.");

                $this->getCache()->clean(array(self::CACHE_TAG));
            }
        } catch (Exception $e) {
            $this->debugLog($e);
        }

        return $this;
    }

    /**
     * Generate and return cache key.
     *
     * @param array $pieces
     * @param boolean $includeCommonData
     * @param bool $includeStoreId
     * @return string
     */
    public function generateCacheKey(array $pieces, $includeCommonData = true, $includeStoreId = true)
    {
        if ($includeCommonData) {
            $pieces['cache_type'] = self::CACHE_TAG;
        }

        if ($includeStoreId) {
            $pieces['store_id'] = Mage::helper('hostedflow')->getStoreId();
        }

        return sha1(serialize($pieces));
    }

    /**
     * Encode cache data.
     *
     * @param mixed $data
     * @return string
     */
    public function encode($data)
    {
        return (function_exists('json_encode') && function_exists('json_decode')) ? json_encode($data) : serialize($data);
    }

    /**
     * Decode cache data.
     *
     * @param string $data
     * @param bool $isArray
     * @return mixed
     */
    public function decode($data, $isArray = false)
    {
        $result = null;

        if ($data) {
            if ($this->isSerialized($data)) {
                $result = @unserialize($data);
            } else {
                if (function_exists('json_decode')) {
                    $result = @json_decode($data, $isArray);
                }
            }
        }

        return $result;
    }

    /**
     * Check if a string is serialized.
     *
     * @param string $val
     * @return boolean
     */
    public function isSerialized($val)
    {
        $result = false;

        if (is_string($val) && !empty($val)) {
            $val = @unserialize($val);
            $result = !($val === false && $val != 'b:0;');
        }

        return $result;
    }

    /**
     * Log debug message.
     *
     * @param string|array|Exception $message
     * @return $this
     * @throws Exception
     */
    public function debugLog($message)
    {
        Mage::helper('hostedflow/log')->entry($message, Resursbank_Hostedflow_Helper_Log::CACHE_LOG);

        return $this;
    }

    /**
     * Retrieve cache lifetime (in seconds).
     *
     * @return int
     */
    public function getLifetime()
    {
        return (int)Mage::getStoreConfig('payment/hostedflow_standard/cache_lifetime');
    }

}
