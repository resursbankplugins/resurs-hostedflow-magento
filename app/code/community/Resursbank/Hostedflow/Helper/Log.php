<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Helper_Log extends Mage_Core_Helper_Abstract
{

    /**
     * General log file.
     */
    const GENERAL_LOG = 'resursbank_general.log';

    /**
     * General log file.
     */
    const CACHE_LOG = 'resursbank_cache.log';

    /**
     * Log something.
     *
     * @param string|array|Exception $message
     * @param null|string $file
     * @return $this
     */
    public function entry($message, $file = null)
    {
        $file = (string)$file;

        if (empty($file)) {
            $file = self::GENERAL_LOG;
        }

        if (Mage::getStoreConfigFlag('payment/hostedflow_standard/debug')) {
            if ($message instanceof Exception) {
                $message = $message->getFile() . ' :: ' . $message->getLine() . ' - ' . $message->getMessage();
            }

            Mage::log((string)$message, null, $file, true);
        }

        return $this;
    }

}
