<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once(Mage::getBaseDir() . '/lib/Resursbank/Hostedflow/RBEcomPHP/classes/rbapiloader.php');

/**
 * Class Resursbank_Hostedflow_Helper_Ecom
 */
class Resursbank_Hostedflow_Helper_Ecom extends Resursbank_Hostedflow_Helper_Data
{

    /**
     * @var \Resursbank\RBEcomPHP\ResursBank
     */
    private $connection;

    /**
     * @return \Resursbank\RBEcomPHP\ResursBank
     */
    public function getConnection()
    {
        if (is_null($this->connection)) {
            $this->connection = new \Resursbank\RBEcomPHP\ResursBank($this->getApi()->getUsername(), $this->getApi()->getPassword());
        }

        return $this->connection;
    }

    /**
     * Check if ECom integration is enabled.
     *
     * @param int|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null)
    {
        return (parent::isEnabled($storeId) && Mage::getStoreConfigFlag('payment/hostedflow_standard/ecom_enabled', (is_null($storeId) ? $this->getStoreId() : $storeId)));
    }

    /**
     * Check if a provided payment session has a certain status applied.
     *
     * @param \Resursbank\RBEcomPHP\resurs_payment $session
     * @param string $status
     * @return bool
     */
    public function paymentSessionHasStatus(\Resursbank\RBEcomPHP\resurs_payment $session, $status)
    {
        $status = strtoupper((string) $status);

        return (isset($session->status) &&
            ((is_array($session->status) && in_array($status, $session->status)) || (is_string($session->status) && strtoupper($session->status) === $status)));
    }

}
