<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Model_System_Config_Source_Partpayment_Duration
{

    /**
     * Retrieve array of all available number of months for part payments, based on the configured part payment method
     * for suggestion prices.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();

        try {
            /** @var string $paymentMethod */
            $paymentMethod = $this->_getHelper()->getSuggestedPartPaymentMethod();

            if (!empty($paymentMethod)) {
                /** @var array $response */
                $response = $this->_getHelper()->getApi()->getAnnuityFactors(array(
                    'paymentMethodId' => $paymentMethod
                ));

                if (count($response)) {
                    /** @var array $method */
                    foreach ($response as $method) {
                        if (is_array($method) && isset($method['duration'])) {
                            $result[] = array(
                                'value' => $method['duration'],
                                'label' => $method['duration']
                            );
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->_getHelper()->debugLog($e);
        }

        return $result;
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
