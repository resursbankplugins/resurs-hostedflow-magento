<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Model_Payment_Standard extends Mage_Payment_Model_Method_Abstract
{

    /**
     * Unique payment method identifier.
     *
     * @var string
     */
    protected $_code = 'hostedflow_standard';

    /**
     * Block class which renders payment method form (in frontend checkout).
     *
     * @var string
     */
    protected $_formBlockType = 'hostedflow/payment_standard_form';

    /**
     * Block class which renders payment method summary information (in the right-hand sidebar at checkout).
     *
     * @var string
     */
    protected $_infoBlockType = 'hostedflow/payment_standard_info';

    /**
     * Can capture payment.
     *
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Can be refunded.
     *
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * Can refund partial invoice amount.
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * Can be used during checkout.
     *
     * @var bool
     */
    protected $_canUseCheckout = true;

    /**
     * Cannot be used from the administration panel.
     *
     * @var bool
     */
    protected $_canUseInternal = true;

    /**
     * The URL which clients will be redirected to for payment completion.
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('resursbank');
    }

    /**
     * Check whether payment method can be used
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        $result = parent::isAvailable($quote);

        try {
            if ($result) {
                $methods = $this->_getHelper()->getPaymentMethods();

                $result = (count($methods) > 0);
            }
        } catch (Exception $e) {
            $result = false;

            $this->_getHelper()->debugLog($e);
        }

        return $result;
    }

    /**
     * Capture payment.
     *
     * @param Varien_Object $payment
     * @param float $amount
     * @return $this
     * @throws Exception
     */
    public function capture(Varien_Object $payment, $amount)
    {
        parent::capture($payment, $amount);

        try {
            /** @var Mage_Sales_Model_Order $order */
            $order = $payment->getOrder();

            if ($this->_getEcomHelper()->isEnabled() && ($order instanceof Mage_Sales_Model_Order) && $order->getId()) {
                // Order reference.
                $reference = $order->getIncrementId();

                // Log capturing event.
                $this->_getHelper()->debugLog("Capturing payment of order {$reference} with amount {$amount}, using method {$this->_code}.");

                // Finalize the payment.
                $this->finalizePayment($reference);

                // Set order increment_id as transaction identifier.
                $payment->setTransactionId($reference);

                // Log capturing event.
                $this->_getHelper()->debugLog("Successfully captured payment of order {$reference}.");

                // Add history entry.
                $order->addStatusHistoryComment($this->_getHelper()->__('Resursbank: payment debited.'));

                // Close transaction to complete process.
                $payment->setIsTransactionClosed(true);
            }
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($this->_getHelper()->__('Failed to debit Resursbank payment.'));
            $this->_getHelper()->debugLog($e);
            throw $e;
        }

        return $this;
    }

    /**
     * Finalize Resursbank payment.
     *
     * @param string $reference
     * @return $this
     * @throws Exception
     */
    public function finalizePayment($reference)
    {
        $reference = (string) $reference;

        /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
        $connection = $this->_getEcomHelper()->getConnection();

        /** @var \Resursbank\RBEcomPHP\resurs_payment $paymentSession */
        $paymentSession = $connection->getPayment($reference);

        if (!($paymentSession instanceof \Resursbank\RBEcomPHP\resurs_payment)) {
            throw new Exception("Resursbank payment {$reference} could not be found.");
        }

        // Finalize Resursbank payment.
        if (!$paymentSession->finalized && !$this->_getEcomHelper()->paymentSessionHasStatus($paymentSession, 'IS_DEBITED')) {
            if ($paymentSession->frozen) {
                throw new Exception("Resursbank payment {$reference} is still frozen.");
            }

            if (!$this->_getEcomHelper()->paymentSessionHasStatus($paymentSession, 'DEBITABLE')) {
                throw new Exception("Resursbank payment {$reference} cannot be debited yet.");
            }

            // Finalize payment session.
            if (!$connection->finalizePayment($reference)) {
                throw new Exception("Failed to finalize Resursbank payment {$reference}");
            }
        }

        return $this;
    }

    /**
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

    /**
     * @return Resursbank_Hostedflow_Helper_Ecom
     */
    public function _getEcomHelper()
    {
        return Mage::helper('hostedflow/ecom');
    }

}
