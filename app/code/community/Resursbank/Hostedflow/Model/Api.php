<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This class contains functions to perform varies calls against a SOAP service available at Resursbank. It can for
 * instance collect available payment methods based on purchase amount.
 *
 * Class Resursbank_Hostedflow_Model_Api_Soap
 */
class Resursbank_Hostedflow_Model_Api extends Mage_Core_Model_Abstract
{

    /**
     * SOAP service live URL.
     */
    const SERVICE_URL = 'https://ecommerce.resurs.com/ws/V4/SimplifiedShopFlowService?wsdl';

    /**
     * SOAP service test URL.
     */
    const SERVICE_TEST_URL = 'https://test.resurs.com/ecommerce-test/ws/V4/SimplifiedShopFlowService?wsdl';

    /**
     * SOAP service URL.
     *
     * @var string
     */
    private $serviceUrl;

    /**
     * SOAP service connection client.
     *
     * @var SoapClient
     */
    private $connection;

    /**
     * Retrieve URL to perform gateway related calls against.
     *
     * @return string
     */
    private function getServiceUrl()
    {
        if (is_null($this->serviceUrl)) {
            $this->serviceUrl = $this->_getHelper()->isTestMode() ? self::SERVICE_TEST_URL : self::SERVICE_URL;
        }

        return $this->serviceUrl;
    }

    /**
     * Get instance of SoapClient for API calls.
     *
     * @return SoapClient
     */
    private function getConnection()
    {
        if (is_null($this->connection)) {
            $this->connection = new SoapClient(
                $this->getServiceUrl(),
                array(
                    'exceptions' => 1,
                    'connection_timeout' => $this->getConnectionTimeout(),
                    'login' => $this->getUsername(),
                    'password' => $this->getPassword(),
                    'trace' => 1
                )
            );
        }

        return $this->connection;
    }

    /**
     * Perform SOAP call.
     *
     * @param string $method
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    private function call($method, array $parameters = array())
    {
        $method = (string)$method;

        // Perform API call.
        $response = $this->getConnection()->__soapCall($method, array($parameters));

        // Basic response validation.
        if (!is_object($response) || !isset($response->return)) {
            throw new Exception('Unexpected return value from method ' . $method . ' using parameters ' . json_encode($parameters));
        }

        return $this->prepareResponse($response->return);
    }

    /**
     * Convert stdClass architecture to associative array.
     *
     * @param mixed $response
     * @return array
     */
    private function prepareResponse($response)
    {
        $result = $response;

        if (is_array($result)) {
            // @todo perhaps there is a more optimised way of doing this?
            $result = @json_decode(@json_encode($response), true);
        }

        return $result;
    }

    /**
     * Retrieve available payment methods from Resursbank's API.
     *
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    public function getPaymentMethods(array $parameters = array())
    {
        // Validate provided parameters.
        $this->validatePaymentMethodsParameters($parameters);

        $result = $this->call('getPaymentMethods', $parameters);

        return is_array($result) ? $result : array();
    }

    /**
     * Validate parameters used when retrieving payment methods from the API.
     *
     * @param array $parameters
     * @return $this
     * @throws Exception
     */
    public function validatePaymentMethodsParameters(array $parameters = array())
    {
        if (count($parameters)) {
            foreach ($parameters as $key => $value) {
                if (!in_array($key, array('language', 'customerType', 'purchaseAmount'))) {
                    throw new Exception("Invalid parameter {$key} supplied.");
                }

                if ($key === 'language' && (!is_string($value) || strlen($value) > 2)) {
                    throw new Exception("Illegal language {$value} provided.");
                }

                if ($key === 'purchaseAmount' && !is_numeric($value)) {
                    throw new Exception("Illegal purchaseAmount {$value} provided.");
                }

                if ($key === 'customerType' && !in_array($value, array('NATURAL', 'LEGAL'))) {
                    throw new Exception("Illegal customer type {$value} provided.");
                }
            }
        }

        return $this;
    }

    /**
     * This method retrieve all possible number of moths payments can be based on (part payments for 12, 24, 72 months
     * etc.).
     *
     * @param array $parameters (requires at least paymentMethodId)
     * @return array
     * @throws Exception
     */
    public function getAnnuityFactors(array $parameters)
    {
        // Validate provided parameters.
        $this->validateAnnuityFactorsParameters($parameters);

        $result = $this->call('getAnnuityFactors', $parameters);

        return is_array($result) ? $result : array();
    }

    /**
     * Validate parameters used when retrieving annuity factors from the API.
     *
     * @param array $parameters
     * @return $this
     * @throws Exception
     */
    public function validateAnnuityFactorsParameters(array $parameters = array())
    {
        if (count($parameters)) {
            foreach ($parameters as $key => $value) {
                if (!in_array($key, array('paymentMethodId'))) {
                    throw new Exception("Invalid parameter {$key} supplied.");
                }

                if ($key === 'paymentMethodId') {
                    $this->validatePaymentMethodId($value);
                }
            }
        }

        return $this;
    }

    /**
     * Retrieve the part payment information text (and price table) from Resursbank based on provided parameters.
     *
     * @param array $parameters
     * @return string
     * @throws Exception
     */
    public function getPartPaymentInformation(array $parameters)
    {
        // Validate provided parameters.
        $this->validatePartPaymentInformationParameters($parameters);

        $result = $this->call('getCostOfPurchaseHtml', $parameters);

        return is_string($result) ? $result : '';
    }

    /**
     * Validate parameters used when retrieving part payment information from the API.
     *
     * @param array $parameters
     * @return $this
     * @throws Exception
     */
    public function validatePartPaymentInformationParameters(array $parameters = array())
    {
        if (count($parameters)) {
            foreach ($parameters as $key => $value) {
                if (!in_array($key, array('paymentMethodId', 'amount'))) {
                    throw new Exception("Invalid parameter {$key} supplied.");
                }

                if ($key === 'paymentMethodId') {
                    $this->validatePaymentMethodId($value);
                }

                if ($key === 'amount' && !is_numeric($value)) {
                    throw new Exception("Illegal amount {$value} provided.");
                }
            }
        }

        return $this;
    }

    /**
     * Validate payment method id.
     *
     * @param string $id
     * @return $this
     * @throws Exception
     */
    public function validatePaymentMethodId($id)
    {
        if (!is_string($id)) {
            throw new Exception("Illegal payment method {$id} provided.");
        }

        $methods = $this->_getHelper()->getPaymentMethods(array(), $id);

        if (!count($methods)) {
            throw new Exception("Illegal payment method {$id} provided.");
        }

        return $this;
    }

    /**
     * Retrieve username used when performing calls against Resursbank's API (CURL and SOAP based calls).
     *
     * @return string
     */
    public function getUsername()
    {
        return (string)Mage::getStoreConfig('payment/hostedflow_standard/api_username');
    }

    /**
     * Retrieve password used when performing calls against Resursbank's API (CURL and SOAP based calls).
     *
     * @return string
     */
    public function getPassword()
    {
        return (string)Mage::getStoreConfig('payment/hostedflow_standard/api_password');
    }

    /**
     * Retrieve number of seconds before API requests timeout.
     *
     * @return int
     */
    public function getConnectionTimeout()
    {
        return (int)Mage::getStoreConfig('payment/hostedflow_standard/api_connection_timeout');
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
