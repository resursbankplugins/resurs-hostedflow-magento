<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Basically this class contains functions to collect information and perform a CURL call to retrieve the gateway URL
 * used by the client to perform payment of an order.
 *
 * Class Resursbank_Hostedflow_Model_Gateway
 */
class Resursbank_Hostedflow_Model_Gateway extends Mage_Core_Model_Abstract
{

    /**
     * Live URL.
     */
    const SERVICE_URL = 'https://ecommerce-hosted.resurs.com/back-channel';

    /**
     * Test URL.
     */
    const SERVICE_TEST_URL = 'https://test.resurs.com/ecommerce-test/hostedflow/back-channel';

    /**
     * URL used when performing gateway related calls.
     *
     * @var string
     */
    protected $serviceUrl;

    /**
     * Retrieve URL to perform gateway related calls against.
     *
     * @return string
     */
    public function getServiceUrl()
    {
        if (is_null($this->serviceUrl)) {
            $this->serviceUrl = $this->_getHelper()->isTestMode() ? self::SERVICE_TEST_URL : self::SERVICE_URL;
        }

        return $this->serviceUrl;
    }

    /**
     * Retrieves a dynamic URL to Resursbank's gateway (the URL is unique for every individual payment).
     *
     * @param Mage_Sales_Model_Order $order
     * @return mixed
     */
    public function getUrl(Mage_Sales_Model_Order $order)
    {
        // Retrieve URL from Resursbank.
        $curl = curl_init();

        // Data posted to API.
        $payload = json_encode($this->getPayload($order));

        // Track data in log.
        $this->_getHelper()->debugLog('Using URL: ' . $this->getServiceUrl());
        $this->_getHelper()->debugLog('Payload: ' . $payload);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->getServiceUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_HEADER => false,
            CURLOPT_USERPWD => $this->_getHelper()->getApi()->getUsername() . ':' . $this->_getHelper()->getApi()->getPassword(),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'HTTP POST ' . $this->getServiceUrl()
            )
        ));

        // Perform CURL call.
        $response = curl_exec($curl);

        // And more logs.
        $this->_getHelper()->debugLog('Response: ' . json_encode($response));

        return $response;
    }

    /**
     * Collects information used by the getUrl() function to retrieve the payment gateway URL.
     *
     * @param Mage_Sales_Model_Order $order
     * @return string
     * @throws Exception
     */
    public function getPayload(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());

        if (!$quote->getId()) {
            throw new Exception('No quote available for order ' . $order->getId());
        }

        $result = array(
            'paymentData' => array(
                'paymentMethodId' => $order->getData('hostedflow_payment_method_id'),
                'preferredId' => $order->getIncrementId()
            ),
            'orderData' => $this->getOrderData($quote),
            'customer' => $this->getCustomerData($quote),
            'waitForFraudControl' => true,
            'annulIfFrozen' => false,
            'finalizeIfBooked' => false,
            'successUrl' => Mage::getUrl('resursbank/index/success/token/' . $order->getData('hostedflow_token')),
            'failUrl' => Mage::getUrl('resursbank/index/fail/token/' . $order->getData('hostedflow_token')),
            'backUrl' => Mage::getUrl('resursbank/index/back/token/' . $order->getData('hostedflow_token'))
        );

        return $result;
    }

    /**
     * Retrieve order data from quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getOrderData(Mage_Sales_Model_Quote $quote)
    {
        return array(
            'orderLines' => $this->getOrderLines($quote),
            'totalAmount' => $quote->getGrandTotal(),
            'totalVatAmount' => $quote->getShippingAddress()->getTaxAmount()
        );
    }

    /**
     * Retrieve order lines submitted to the API with initialize / update requests.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getOrderLines(Mage_Sales_Model_Quote $quote)
    {
        $data = $this->getProductLines($quote);

        // Add discount row to order lines.
        $discount = $this->getDiscountLine($quote);

        if (count($discount)) {
            $data[] = $discount;
        }

        // Add shipping cost row to order lines.
        $shipping = $this->getShippingLine($quote);

        if (count($shipping)) {
            $data[] = $shipping;
        }

        return $data;
    }

    /**
     * Retrieve array of all order lines in quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     * @throws Exception
     */
    public function getProductLines(Mage_Sales_Model_Quote $quote)
    {
        $result = array();

        $items = $quote->getAllItems();

        if (!count($items)) {
            throw new Exception('No items in shopping cart.');
        }

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($items as $item) {
            if ($this->validateProductLine($item)) {
                $result[] = $this->getProductLine($item);
            }
        }

        return $result;
    }

    /**
     * Convert Mage_Sales_Model_Quote_Item to an order line for the API.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return array
     */
    public function getProductLine(Mage_Sales_Model_Quote_Item $item)
    {
        return array(
            'artNo' => $item->getSku(),
            'description' => $item->getName(),
            'quantity' => (float)$item->getQty(),
            'unitAmountWithoutVat' => (float)$item->getPrice(),
            'vatPct' => $this->getItemTaxPercent($item),
            'totalVatAmount' => ((float)$item->getRowTotalInclTax() - (float)$item->getRowTotal()), // This leads to _getData('tax_amount') so we can safely ignore it being deprecated.
            'totalAmount' => (float)$item->getRowTotalInclTax()
        );
    }

    /**
     * Retrieve item tax percent. Certain product types will not include the tax_percent property, and in those cases
     * we must calculate it manually ((tax_amount / price) * 100).
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return float
     */
    public function getItemTaxPercent(Mage_Sales_Model_Quote_Item $item)
    {
        $result = 0;

        if ($item->hasTaxPercent()) {
            $result = (float)$item->getTaxPercent();
        } else {
            $taxAmount = (float)$item->getPriceInclTax() - (float)$item->getPrice();

            if ($taxAmount > 0) {
                $result = (($taxAmount / (float)$item->getPrice()) * 100);
            }
        }

        return (float)$result;
    }

    /**
     * Validate product before including it in product lines sent to Resursbank.
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    public function validateProductLine(Mage_Sales_Model_Quote_Item $item)
    {
        $result = ((float)$item->getQty() > 0 && !$item->getParentItem());

        if ($result && $item->getProductType() === 'configurable') {
            $result = (count($item->getChildren()) > 0);
        }

        return $result;
    }

    /**
     * Retrieve order line for shipping amount.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getShippingLine(Mage_Sales_Model_Quote $quote)
    {
        $result = array();

        $amount = (float)$quote->getShippingAddress()->getShippingAmount();

        if ($amount > 0) {
            $result = array(
                'artNo' => 'shipping',
                'description' => $this->_getHelper()->__('Shipping & Handling'),
                'quantity' => 1,
                'unitAmountWithoutVat' => $amount,
                'vatPct' => $this->getShippingTax($quote),
                'totalVatAmount' => ((float)$quote->getShippingAddress()->getShippingInclTax() - (float)$quote->getShippingAddress()->getShippingAmount()),
                'totalAmount' => (float)$quote->getShippingAddress()->getShippingInclTax()
            );
        }

        return $result;
    }

    /**
     * Retrieve shipping tax percentage from quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return float|int
     */
    public function getShippingTax(Mage_Sales_Model_Quote $quote)
    {
        $result = 0;

        $inclTax = (float)$quote->getShippingAddress()->getShippingInclTax();
        $exclTax = (float)$quote->getShippingAddress()->getShippingAmount();

        if ($inclTax > $exclTax) {
            $result = ((($inclTax / $exclTax) - 1) * 100);
        }

        return $result;
    }

    /**
     * Retrieve order line for discount amount.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getDiscountLine(Mage_Sales_Model_Quote $quote)
    {
        $result = array();

        $amount = (float)$quote->getShippingAddress()->getDiscountAmount();

        if ($amount < 0) {
            $name = 'Discount';
            $code = (string)$quote->getCouponCode();

            if (strlen($code)) {
                $name.= ' (%s)';
            }

            /** @var float $taxPercent */
            $taxPercent = $this->getDiscountTaxPercent($quote);

            /** @var float $taxAmount */
            $taxAmount = abs($this->getDiscountTaxAmount($amount, $taxPercent));

            $result = array(
                'artNo'                 => $code ? $code : 'discount',
                'description'           => $this->_getHelper()->__($name, array($code)),
                'quantity'              => 1,
                'unitAmountWithoutVat'  => ($amount + $taxAmount),
                'vatPct'                => $taxPercent,
                'totalVatAmount'        => $taxAmount,
                'totalAmount'           => $amount
            );
        }

        return $result;
    }

    /**
     * Retrieve discount tax amount.
     *
     * @param float $amount
     * @param float $taxPercent
     * @return int
     */
    public function getDiscountTaxAmount($amount, $taxPercent)
    {
        $result = 0;

        if ($taxPercent > 0) {
            $result = ($amount / (1 + ($taxPercent / 100))) - $amount;
        }

        return $result;
    }

    /**
     * Retrieve discount tax percentage from quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return float|int
     */
    public function getDiscountTaxPercent(Mage_Sales_Model_Quote $quote)
    {
        $result = 0;

        if (Mage::getStoreConfig('tax/calculation/apply_after_discount') === '1') {
            if ($quote->getShippingAddress()->getSubtotalInclTax() > $quote->getShippingAddress()->getSubtotal()) {
                // We are expecting a value like 1.25 - 1 (0.25) here.
                $percent = ($quote->getShippingAddress()->getSubtotalInclTax() / $quote->getShippingAddress()->getSubtotal()) - 1;

                if ($percent > 0 && $percent < 1) {
                    $result = round($percent * 100, 4);
                }
            }
        }

        return $result;
    }

    /**
     * Retrieve customer data from quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getCustomerData(Mage_Sales_Model_Quote $quote)
    {
        return array(
            'address' => $this->getAddressData($quote),
            'phone' => $quote->getBillingAddress()->getTelephone(),
            'email' => $quote->getBillingAddress()->getEmail(),
            'type' => 'NATURAL' // ($quote->getBillingAddress()->getCompany() ? 'LEGAL' : 'NATURAL')  <----  This should work, but we experience problems using the customerType "LEGAL" for some reason.
        );
    }

    /**
     * Retrieve billing address from quote.
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getAddressData(Mage_Sales_Model_Quote $quote)
    {
        return array(
            'fullName' => ($quote->getCustomerFirstname() . ' ' . $quote->getCustomerLastname()),
            'firstName' => $quote->getCustomerFirstname(),
            'lastName' => $quote->getCustomerLastname(),
            'addressRow1' => (string)$quote->getBillingAddress()->getStreet(1),
            'addressRow2' => (string)$quote->getBillingAddress()->getStreet(2),
            'postalArea' => $quote->getBillingAddress()->getCity(),
            'postalCode' => $quote->getBillingAddress()->getPostcode(),
            'countryCode' => $quote->getBillingAddress()->getCountryId()
        );
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
