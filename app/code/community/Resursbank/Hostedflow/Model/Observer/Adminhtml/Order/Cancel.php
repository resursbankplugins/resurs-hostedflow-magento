<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Hostedflow_Model_Observer_Adminhtml_Order_Cancel
 */
class Resursbank_Hostedflow_Model_Observer_Adminhtml_Order_Cancel
{

    /**
     * Annul Resursbank payment after an order has been annulled in Magento.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        if ($this->_getHelper()->isEnabled() && !$this->isOrderEditAction()) {
            /** @var Mage_Sales_Model_Order $order */
            $order = $observer->getOrder();

            try {
                if (!($order instanceof Mage_Sales_Model_Order)) {
                    throw new Exception("No order object provided.");
                }

                /** @var string $reference */
                $reference = $order->getIncrementId();

                $this->_getHelper()->debugLog("Cancelling payment {$reference}");

                /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
                $connection = $this->_getHelper()->getConnection();

                // This cannot be removed, otherwise annulPayment won't work as getPayment stores necessary
                // information on the helper.
                /** @var \Resursbank\RBEcomPHP\resurs_payment $payment */
                $payment = $connection->getPayment($reference);

                if (!($payment instanceof \Resursbank\RBEcomPHP\resurs_payment)) {
                    throw new Exception("Failed to obtain payment object for reference {$reference}");
                }

                // Annul payment.
                if (!$connection->annulPayment($reference)) {
                    throw new Exception('Something went wrong while communicating with the RBECom API.');
                }

                // Log entry.
                $this->_getHelper()->debugLog("Successfully cancelled payment {$reference}");

                // Order history comment.
                $order->addStatusHistoryComment($this->_getHelper()->__("Resursbank: payment annulled."));
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($this->_getHelper()->__('Failed to cancel Resursbank payment.'));
                $this->_getHelper()->debugLog($e);
                throw $e;
            }
        }
    }

    /**
     * Check whether or not we are currently editing an order.
     *
     * @return bool
     */
    public function isOrderEditAction()
    {
        return (Mage::app()->getRequest()->getModuleName() === 'admin' &&
            Mage::app()->getRequest()->getControllerName() === 'sales_order_edit');
    }

    /**
     * @return Resursbank_Hostedflow_Helper_Ecom
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow/ecom');
    }

}
