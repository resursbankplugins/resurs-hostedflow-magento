<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class Resursbank_Hostedflow_Model_Observer_Adminhtml_Order_Creditmemo_Save
 */
class Resursbank_Hostedflow_Model_Observer_Adminhtml_Order_Creditmemo_Save
{

    /**
     * Credit the Resursbank payment when a creditmemo has been created.
     *
     * @param Varien_Event_Observer $observer
     * @throws Exception
     */
    public function execute(Varien_Event_Observer $observer)
    {
        if ($this->_getHelper()->isEnabled()) {
            /** @var Mage_Sales_Model_Order_Creditmemo $order */
            $creditmemo = $observer->getDataObject();

            try {
                if (!($creditmemo instanceof Mage_Sales_Model_Order_Creditmemo)) {
                    throw new Exception("No creditmemo object provided.");
                }

                /** @var string $reference */
                $reference = $creditmemo->getOrder()->getIncrementId();

                $this->_getHelper()->debugLog("Refunding payment {$reference}");

                /** @var \Resursbank\RBEcomPHP\ResursBank $connection */
                $connection = $this->_getHelper()->getConnection();

                // This cannot be removed, otherwise creditPayment won't work as getPayment stores necessary
                // information on the helper.
                /** @var \Resursbank\RBEcomPHP\resurs_payment $payment */
                $payment = $connection->getPayment($reference);

                if (!($payment instanceof \Resursbank\RBEcomPHP\resurs_payment)) {
                    throw new Exception("Failed to obtain payment object for reference {$reference}");
                }

                // Credit payment.
                if (!$connection->creditPayment($reference, $this->getCreditMemoItems($creditmemo), array(), false, true)) {
                    throw new Exception('Something went wrong while communicating with the RBECom API.');
                }

                // Log entry.
                $this->_getHelper()->debugLog("Successfully refunded payment {$reference}");

                // Order history comment.
                $creditmemo->getOrder()->addStatusHistoryComment($this->_getHelper()->__("Resursbank: payment refunded."));
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($this->_getHelper()->__('Failed to credit Resursbank payment.'));
                $this->_getHelper()->debugLog($e);
                throw $e;
            }
        }
    }

    /**
     * Retrieve creditmemo items as an array formatted to function with outgoing Resursbank calls.
     *
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     * @return array
     */
    private function getCreditMemoItems(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $result = array();

        if (count($creditmemo->getAllItems())) {
            /** @var Mage_Sales_Model_Order_Creditmemo_Item $item */
            foreach ($creditmemo->getAllItems() as $item) {
                $result[] = array(
                    'artNo' => $item->getSku(),
                    'quantity' => $item->getQty()
                );
            }

            // Shipping line.
            if ((float) $creditmemo->getShippingAmount() > 0) {
                $result[] = array(
                    'artNo' => 'shipping',
                    'quantity' => 1
                );
            }

            // Discount line.
            $discountAmount = (float) $creditmemo->getDiscountAmount();

            if ($discountAmount < 0) {
                $result[] = array(
                    'artNo' => 'discount',
                    'quantity' => 1
                );
            }
        }

        return $result;
    }

    /**
     * @return Resursbank_Hostedflow_Helper_Ecom
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow/ecom');
    }

}
