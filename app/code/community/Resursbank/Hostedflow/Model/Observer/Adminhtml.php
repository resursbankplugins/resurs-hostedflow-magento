<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Model_Observer_Adminhtml
{

    /**
     * After applying changes to the configuration we should reload all cached data associated with the extension.
     *
     * @param Varien_Event_Observer $observer
     * @todo This could be improved to check if any settings related to the HostedFlow module have changed before we refresh the cache. Right now it will refresh whenever you change any setting.
     */
    public function cleanCache(Varien_Event_Observer $observer)
    {
        Mage::helper('hostedflow/cache')->clean();
    }

}
