<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Model_Observer
{

    /**
     * On for example the bundled product view the price blocks are included multiple times (for whatever reason). To
     * avoid us injecting the part payment price suggestion widget multiple times we utilise the registry, using this
     * key.
     */
    const UNIQUE_WIDGET_REGISTRY_KEY = 'resursbank_hostedflow_partpayment_widget';

    /**
     * Append selected payment method id for HostFlow before saving quotes.
     *
     * @param Varien_Event_Observer $observer
     */
    public function appendHostedFlowPaymentMethodIdToQuote(Varien_Event_Observer $observer)
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                /** @var Mage_Sales_Model_Quote $quote */
                $quote = $observer->getDataObject();

                /** @var array $info */
                $info = Mage::app()->getRequest()->getPost('payment');

                if (
                    is_array($info) &&
                    ($quote instanceof Mage_Sales_Model_Quote) &&
                    !$quote->getReservedOrderId()
                ) {
                    $quote->setData('hostedflow_payment_method_id', ((isset($info['hostedflow_payment_method_id']) && !empty($info['hostedflow_payment_method_id'])) ? $info['hostedflow_payment_method_id'] : null));
                }
            } catch (Exception $e) {
                $this->_getHelper()->debugLog($e->getMessage());
            }
        }
    }

    /**
     * Append selected payment method id for HostedFlow before saving order. We need this later when communicating with
     * the gateway to complete the payment.
     *
     * We also append a token which will be used to identify the order during callbacks from Resursbank.
     *
     * @param Varien_Event_Observer $observer
     */
    public function appendHostedFlowDataToOrder(Varien_Event_Observer $observer)
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                /** @var Mage_Sales_Model_Order $order */
                $order = $observer->getDataObject();

                if (
                    ($order instanceof Mage_Sales_Model_Order) &&
                    $order->getPayment()->getMethod() === 'hostedflow_standard' &&
                    !$order->hasData('hostedflow_token')
                ) {
                    /** @var Mage_Sales_Model_Quote $quote */
                    $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());

                    if ($quote && $quote->hasData('hostedflow_payment_method_id')) {
                        $order->setData('hostedflow_payment_method_id', $quote->getData('hostedflow_payment_method_id'))
                            ->setData('hostedflow_token', Mage::helper('hostedflow')->strRand(64));
                    }
                }
            } catch (Exception $e) {
                $this->_getHelper()->debugLog($e->getMessage());
            }
        }
    }

    /**
     * This will append the part payment information widget to the original price block displayed on the product view.
     *
     * @param Varien_Event_Observer $observer
     */
    public function appendPartPaymentInformationToPriceBlock(Varien_Event_Observer $observer)
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                /* @var Mage_Core_Block_Abstract $block */
                $block = $observer->getEvent()->getBlock();

                if (
                    (($block instanceof Mage_Catalog_Block_Product_Price) || ($block instanceof Mage_Bundle_Block_Catalog_Product_Price)) &&
                    $this->_getHelper()->isActivePath('catalog/product/view')
                ) {
                    /** @var Resursbank_Hostedflow_Block_Catalog_Product_View_Partpayment_Widget $widget */
                    $widget = Mage::app()->getLayout()->createBlock('hostedflow/catalog_product_view_partpayment_widget');

                    if ((Mage::registry(self::UNIQUE_WIDGET_REGISTRY_KEY) !== true) &&
                        ($widget instanceof Resursbank_Hostedflow_Block_Catalog_Product_View_Partpayment_Widget) &&
                        in_array($block->getTemplate(), array('catalog/product/price.phtml', 'bundle/catalog/product/view/price.phtml'))
                    ) {
                        $observer->getTransport()->setHtml($observer->getTransport()->getHtml() . $widget->toHtml());

                        Mage::register(self::UNIQUE_WIDGET_REGISTRY_KEY, true);
                    }
                }
            } catch (Exception $e) {
                $this->_getHelper()->debugLog($e->getMessage());
            }
        }
    }

    /**
     * Retrieve helper object.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
