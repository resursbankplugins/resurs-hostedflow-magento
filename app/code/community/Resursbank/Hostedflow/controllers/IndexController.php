<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * Redirect user to payment gateway to perform payment after placing order.
     */
    public function indexAction()
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                /** @var Mage_Sales_Model_Order $order */
                $order = $this->_getHelper()->getOrderById(Mage::getSingleton('checkout/session')->getLastOrderId());

                // Retrieve gateway URL from Resursbank.
                $result = $this->_getHelper()->getGateway()->getUrl($order);

                if (!is_string($result) || empty($result)) {
                    throw new Exception('Failed to retrieve gateway URL.');
                }

                // Decode returned value.
                $result = json_decode($result, true);

                if (!is_array($result) || !isset($result['location'])) {
                    throw new Exception('Unexpected answer when retrieving gateway URL.');
                }

                // Redirect to the gateway.
                $this->_redirectUrl($result['location']);
            } catch (Exception $e) {
                // Log error.
                $this->_getHelper()->debugLog($e);

                // Show pretty error message to client.
                Mage::getSingleton('checkout/session')->addError($this->__('Unable to redirect to payment gateway.'));

                // Recreate order if possible, otherwise redirect to the cart.
                if (isset($order)) {
                    $this->_redirect('*/*/back', array('token' => $order->getData('hostedflow_token')));
                } else {
                    $this->_redirect('checkout/cart');
                }
            }
        } else {
            // Since the module is disabled we will redirect to the start page without any information.
            $this->_redirect('/');
        }
    }

    /**
     * This function will be executed in the event that the payment couldn't be successfully completed. If that happens,
     * the order will already have been placed, which means we need to cancel the existing order and recreate it.
     */
    public function backAction()
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                $token = $this->getRequest()->get('token');

                /** @var Mage_Sales_Model_Order $order */
                $order = $this->_getHelper()->getOrderByToken($token);

                if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
                    throw new Exception("Failed to load order with id {$token}");
                }

                // Rebuild cart from order.
                $this->_getHelper()
                    ->rebuildCart($order)
                    ->cancelOrder($order);

                // Leave pretty error message to client.
                Mage::getSingleton('checkout/session')->addNotice($this->__('The payment failed and the order could not be placed. Please try proceeding through the checkout again.'));
            } catch (Exception $e) {
                // Debug log.
                $this->_getHelper()->debugLog($e);

                // Leave client error message.
                Mage::getSingleton('checkout/session')->addError($this->__('The payment failed and the order could not be recreated. Please recreate your order manually and try proceeding through the checkout again.'));

                // Redirect to cart.
                $this->_redirect('checkout/cart');
            }
        } else {
            // Since the module is disabled we will redirect to the start page without any information.
            $this->_redirect('/');
        }
    }

    /**
     * Payment succeeded.
     */
    public function successAction()
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                /** @var Mage_Sales_Model_Order $order */
                $order = $this->_getHelper()->getOrderByToken($this->getRequest()->get('token'));

                // Set order status.
                $status = (string)Mage::getStoreConfig('payment/hostedflow_standard/paid_order_status');

                if (!empty($status)) {
                    $order->setStatus($status)
                        ->save();
                }

                // Redirect to original success page.
                $this->_redirect('checkout/onepage/success');
            } catch (Exception $e) {
                // Log error.
                $this->_getHelper()->debugLog($e);

                // Show pretty error message to end client.
                Mage::getSingleton('checkout/session')->addError($this->__('The payment succeeded and the order has been placed, however something went wrong while marking it as paid. Please contact customer support for assistance.'));

                // Redirect to cart.
                $this->_redirect('checkout/cart');
            }
        } else {
            // Since the module is disabled we will redirect to the start page without any information.
            $this->_redirect('/');
        }
    }

    /**
     * Payment failed.
     */
    public function failAction()
    {
        if ($this->_getHelper()->isEnabled()) {
            try {
                $token = $this->getRequest()->get('token');

                /** @var Mage_Sales_Model_Order $order */
                $order = $this->_getHelper()->getOrderByToken($token);

                if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
                    throw new Exception("Failed to load order with id {$token}");
                }

                // Rebuild cart from order.
                $this->_getHelper()->rebuildCart($order);

                /** @var Mage_Checkout_Model_Type_Onepage $onepage */
                $onepage = Mage::getSingleton('checkout/type_onepage');

                // Set previous shipping method and coupon code.
                $onepage->saveShippingMethod($order->getShippingMethod());
                $onepage->getQuote()->setCouponCode($order->getCouponCode());
                $onepage->getQuote()->collectTotals()->save();

                // Cancel previous order.
                $this->_getHelper()->cancelOrder($order);

                // Add error message explaining the payment failed but they may try a different payment method.
                Mage::getSingleton('checkout/session')->addError(Mage::getStoreConfig('payment/hostedflow_standard/messages_failure'));
            } catch (Exception $e) {
                // Debug log.
                $this->_getHelper()->debugLog($e);

                // Show pretty error message to end client.
                Mage::getSingleton('checkout/session')->addError($this->__('The payment failed and the cart could not be recreated. Please contact customer service for assistance.'));

                // Redirect to original failure page.
                $this->_redirect('checkout/onepage/failure');
            }

            // Redirect to cart.
            $this->_redirect('checkout/cart');
        } else {
            // Since the module is disabled we will redirect to the start page without any information.
            $this->_redirect('/');
        }
    }

    /**
     * Retrieve part payment information.
     */
    public function informationAction()
    {
        if ($this->_getHelper()->isEnabled()) {
            $this->loadLayout();
            $this->renderLayout();
        } else {
            // Since the module is disabled we will redirect to the start page without any information.
            $this->_redirect('/');
        }
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}