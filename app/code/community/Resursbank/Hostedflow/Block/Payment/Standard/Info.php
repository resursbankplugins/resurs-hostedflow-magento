<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Block_Payment_Standard_Info extends Mage_Payment_Block_Info
{

    /**
     * Prepare information specific to current payment method. We add the selected payment method to the order summary
     * here, if we can obtain it.
     *
     * @param Varien_Object|array $transport
     * @return Varien_Object
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        $result = parent::_prepareSpecificInformation($transport);

        try {
            // Get code of selected Resursbank method.
            $code = Mage::getSingleton('checkout/session')->getQuote() ? Mage::getSingleton('checkout/session')->getQuote()->getData('hostedflow_payment_method_id') : '';

            if (!empty($code)) {
                /** @var array $method */
                $method = $this->_getHelper()->getPaymentMethods(array(
                    'purchaseAmount' => Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal()
                ), $code);

                if (
                    is_array($method) &&
                    isset($method[0]) &&
                    is_array($method[0]) &&
                    isset($method[0]['description'])
                ) {
                    // This text will be displayed in the order summary.
                    $result->setData($this->_getHelper()->__('Method'), $method[0]['description']);
                }
            }
        } catch (Exception $e) {
            $this->_getHelper()->debugLog($e);
        }

        return $result;
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
