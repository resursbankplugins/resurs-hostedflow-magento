<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Block_Catalog_Product_View_Partpayment_Widget extends Mage_Core_Block_Template
{

    /**
     * @var float
     */
    protected $productPrice;

    public function __construct()
    {
        parent::__construct();

        // Cache block output.
        if ($this->_getHelper()->getCache()->isEnabled()) {
            $productId = ($this->getProduct() ? (int)$this->getProduct()->getId() : 0);

            if ($productId > 0) {
                $this->addData(array(
                    'cache_lifetime' => $this->_getHelper()->getCache()->getLifetime(),
                    'cache_tags' => array(Resursbank_Hostedflow_Helper_Cache::CACHE_TAG, (Mage_Catalog_Model_Product::CACHE_TAG . '_' . $productId)),
                    'cache_key' => $this->_getHelper()->getCache()->generateCacheKey(array(
                        'product_id' => $productId
                    ))
                ));
            }
        }

        $this->setTemplate('resursbank/hostedflow/catalog/product/view/widget.phtml');
    }

    /**
     * Retrieve current product model
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Retrieve suggested part payment price based on product price.
     *
     * @return string
     */
    public function getSuggestedPartPaymentPrice()
    {
        $result = '';

        $price = $this->getProductPrice();

        if ($price > 0) {
            $suggestedPrice = $this->_getHelper()->getSuggestedPartPaymentPrice($this->getProductPrice());

            if ($suggestedPrice > 0) {
                $result = Mage::helper('core')->formatCurrency($suggestedPrice);
            }
        }

        return $result;
    }

    /**
     * Retrieve base price for calculating suggested product price.
     *
     * @return float
     */
    public function getProductPrice()
    {
        if (is_null($this->productPrice)) {
            $result = 0;

            if (in_array($this->getProduct()->getTypeId(), array('simple', 'configurable'))) {
                /** @var Mage_CatalogInventory_Model_Stock_Item $item */
                $item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($this->getProduct());

                if ($item->getId()) {
                    $result = $this->getProduct()->getFinalPrice($item->getMinQty());
                }
            } else if ($this->getProduct()->getTypeId() === 'bundle') {
                // @todo This can return am array and I'm not sure how we should handle those scenarios. The if-statement below is a temporary solution.
                $result = Mage::getModel('bundle/product_price')->getTotalPrices($this->getProduct(), 'min', 1);

                if (!is_numeric($result)) {
                    $result = 0;
                }
            }

            $this->productPrice = (float)$result;
        }

        return (float)$this->productPrice;
    }

    /**
     * Retrieve URL to part payment information page.
     *
     * @return string
     */
    public function getPartPaymentInformationUrl()
    {
        return (Mage::getUrl('resursbank/index/information', array(
            'method' => $this->_getHelper()->getSuggestedPartPaymentMethod(),
            'amount' => $this->getProductPrice()
        )));
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
