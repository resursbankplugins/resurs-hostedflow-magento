<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Block_Information extends Mage_Core_Block_Text
{

    public function __construct()
    {
        parent::__construct();

        // Cache block output.
        if ($this->_getHelper()->getCache()->isEnabled()) {
            $this->addData(array(
                'cache_lifetime' => $this->_getHelper()->getCache()->getLifetime(),
                'cache_tags' => array(Resursbank_Hostedflow_Helper_Cache::CACHE_TAG),
                'cache_key' => $this->_getHelper()->getCache()->generateCacheKey(array(
                    Mage::app()->getRequest()->getParam('paymentMethodId'),
                    Mage::app()->getRequest()->getParam('amount')
                ))
            ));
        }
    }

    /**
     * Retrieve part payment table HTML.
     *
     * @return string
     */
    public function getText()
    {
        try {
            $result = Mage::helper('hostedflow')->getApi()->getPartPaymentInformation(array(
                'paymentMethodId' => (string)Mage::app()->getRequest()->getParam('method'),
                'amount' => (float)Mage::app()->getRequest()->getParam('amount')
            ));
        } catch (Exception $e) {
            $this->_getHelper()->debugLog($e);
        }

        return (isset($result) && is_string($result)) ? $result : '';
    }

    /**
     * Retrieve helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
