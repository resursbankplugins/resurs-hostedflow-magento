<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Rewrite of Mage_Adminhtml_Block_Sales_Order_View_Info. This class allows us to include custom HTML on the order view
 * in the admin panel.
 *
 * Class Resursbank_Hostedflow_Block_Adminhtml_Sales_Order_View_Info
 */
class Resursbank_Hostedflow_Block_Adminhtml_Sales_Order_View_Info extends Mage_Adminhtml_Block_Sales_Order_View_Info
{

    /**
     * Render block HTML.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $result = '';

        if ($this->_getHelper()->isEnabled()) {
            try {
                if ($this->isOrderView() || $this->isInvoiceView()) {
                    $result = Mage::getBlockSingleton('hostedflow/adminhtml_sales_order_view_info_payment')
                        ->setOrder($this->getParentBlock()->getOrder())
                        ->toHtml();
                }
            } catch (Exception $e) {
                // Debug log.
                $this->_getHelper()->debugLog($e);
            }
        }

        $result .= parent::_toHtml();

        return $result;
    }

    /**
     * Check if we are on order view in the administration panel.
     *
     * @return bool
     */
    public function isOrderView()
    {
        return (
            Mage::app()->getRequest()->getModuleName() === 'admin' &&
            Mage::app()->getRequest()->getControllerName() === 'sales_order' &&
            Mage::app()->getRequest()->getActionName() === 'view'
        );
    }

    /**
     * Check if we are on invoice view in the administration panel.
     *
     * @return bool
     */
    public function isInvoiceView()
    {
        return (
            Mage::app()->getRequest()->getModuleName() === 'admin' &&
            Mage::app()->getRequest()->getControllerName() === 'sales_order_invoice' &&
            Mage::app()->getRequest()->getActionName() === 'view'
        );
    }

    /**
     * Retrieve general helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
