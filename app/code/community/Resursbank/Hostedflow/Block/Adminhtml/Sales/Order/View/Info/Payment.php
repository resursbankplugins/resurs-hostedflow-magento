<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Resursbank_Hostedflow_Block_Adminhtml_Sales_Order_View_Info_Payment extends Mage_Core_Block_Template
{

    /**
     * Payment information.
     *
     * @var array
     */
    private $_paymentInfo;

    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('resursbank/hostedflow/sales/order/info/payment.phtml');
    }

    /**
     * Get payment status.
     *
     * @return string
     */
    public function getStatus()
    {
        $result = '';

        $status = $this->getPaymentInformation('status');

        if (is_string($status)) {
            $result = $this->__($status);
        } else if (is_array($status) && count($status)) {
            foreach ($status as $piece) {
                $result.= !empty($result) ? " | {$this->__($piece)}" : $this->__($piece);
            }
        }

        if (empty($result)) {
            $result = $this->__('PENDING');
        }

        return $result;
    }

    /**
     * Retrieve order reference.
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->getPaymentInformation('id');
    }

    /**
     * Retrieve payment total.
     *
     * @return float
     */
    public function getPaymentTotal()
    {
        return (float) $this->getPaymentInformation('totalAmount');
    }

    /**
     * Retrieve payment limit.
     *
     * @return float
     */
    public function getPaymentLimit()
    {
        return (float) $this->getPaymentInformation('limit');
    }

    /**
     * Check if payment is frozen.
     *
     * @return bool
     */
    public function isFrozen()
    {
        return ($this->getPaymentInformation('frozen') === true);
    }

    /**
     * Check if payment is fraud marked.
     *
     * @return bool
     */
    public function isFraud()
    {
        return ($this->getPaymentInformation('fraud') === true);
    }

    /**
     * Retrieve full customer name attached to payment.
     *
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->getCustomerInformation('fullName', true);
    }

    /**
     * Retrieve full customer address attached to payment.
     *
     * @return mixed
     */
    public function getCustomerAddress()
    {
        $street = $this->getCustomerInformation('addressRow1', true);
        $street2 = $this->getCustomerInformation('addressRow2', true);
        $postal = $this->getCustomerInformation('postalCode', true);
        $city = $this->getCustomerInformation('postalArea', true);
        $country = $this->getCustomerInformation('country', true);

        $result = "{$street}<br />";

        if ($street2) {
            $result.= "{$street2}<br />";
        }

        $result.= "{$city}<br />";
        $result.= "{$country} - {$postal}";

        return $result;
    }

    /**
     * Retrieve customer telephone number attached to payment.
     *
     * @return mixed
     */
    public function getCustomerTelephone()
    {
        return $this->getCustomerInformation('telephone');
    }

    /**
     * Retrieve customer email attached to payment.
     *
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->getCustomerInformation('email');
    }

    /**
     * Retrieve customer information from Resursbank payment.
     *
     * @param string $key
     * @param bool $address
     * @return mixed
     */
    public function getCustomerInformation($key = '', $address = false)
    {
        $result = (array) $this->getPaymentInformation('customer');

        if ($address) {
            $result = (is_array($result) && isset($result['address'])) ? (array) $result['address'] : null;
        }

        if (!empty($key)) {
            $result = isset($result[$key]) ? $result[$key] : null;
        }

        return $result;
    }

    /**
     * Retrieve payment information from Resursbank.
     *
     * @param string $key
     * @return mixed|null|stdClass
     */
    public function getPaymentInformation($key = '')
    {
        $result = null;

        $key = (string) $key;

        if (is_null($this->_paymentInfo)) {
            try {
                $this->_paymentInfo = (array) $this->_getHelper()->getPayment($this->getOrder());
            } catch (Exception $e) {
                // Avoid any further calls to fetch payment information.
                $this->_paymentInfo = array();

                // Clear any errors from the session since they will otherwise show up on the next page load.
                Mage::getSingleton('core/session')->getMessages(true);
            }
        }

        if (empty($key)) {
            $result = $this->_paymentInfo;
        } else if (is_array($this->_paymentInfo) && isset($this->_paymentInfo[$key])) {
            $result = $this->_paymentInfo[$key];
        }

        return $result;
    }

    /**
     * Retrieve order object.
     *
     * @return Mage_Sales_Model_Order
     */
    public function setOrder(Mage_Sales_Model_Order $order)
    {
        return $this->setData('order', $order);
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->_getData('order');
    }

    /**
     * Retrieve general helper.
     *
     * @return Resursbank_Hostedflow_Helper_Data
     */
    public function _getHelper()
    {
        return Mage::helper('hostedflow');
    }

}
