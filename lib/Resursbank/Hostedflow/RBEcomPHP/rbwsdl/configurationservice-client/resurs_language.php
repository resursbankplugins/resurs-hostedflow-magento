<?php

namespace Resursbank\RBEcomPHP;

if (!class_exists("\\Resursbank\\RBEcomPHP\\resurs_language", false))
{
class resurs_language
{
    const __default = 'sv';
    const sv = 'sv';
    const no = 'no';
    const da = 'da';
    const fi = 'fi';


}

}
