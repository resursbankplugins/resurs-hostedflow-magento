<?php

namespace Resursbank\RBEcomPHP;

if (!class_exists("\\Resursbank\\RBEcomPHP\\resurs_setInvoiceSequence", false))
{
class resurs_setInvoiceSequence
{

    /**
     * @var int $nextInvoiceNumber
     * @access public
     */
    public $nextInvoiceNumber = null;

    /**
     * @access public
     */
    public function __construct()
    {
    
    }

}

}
