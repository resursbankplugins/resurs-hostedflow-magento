<?php

namespace Resursbank\RBEcomPHP;

if (!class_exists("\\Resursbank\\RBEcomPHP\\resurs_invoiceDeliveryTypeEnum", false))
{
class resurs_invoiceDeliveryTypeEnum
{
    const __default = 'NONE';
    const NONE = 'NONE';
    const EMAIL = 'EMAIL';
    const POSTAL = 'POSTAL';


}

}
